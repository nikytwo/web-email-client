package com.summeryrain.email;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import javax.mail.*;
import javax.mail.internet.MimeUtility;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

/**
 * Created by laijie on 15-6-25.
 */
public class ClientTest {
    private  Properties properties;
    private static Option option;

    @Before
    public void setUp() {
        properties = new Properties();
        option = Option.getOpt();
    }

    @Test
    public void testMain() throws MessagingException, UnsupportedEncodingException {
        Session session = Session.getDefaultInstance(properties);
        Store store = session.getStore(option.getProtocol());
        try {
            store.connect(option.getHost(), option.getUsername(), option.getPassword());
            Folder folder = store.getDefaultFolder();
            expandFolder(folder, 0);
        } finally {
            store.close();
        }

    }

    private void expandFolder(Folder folder, int l) throws MessagingException, UnsupportedEncodingException {
        String pre = StringUtils.leftPad("|- ", l++ * 4 + 3);
        System.out.println(pre + folder.getFullName());
        if ((folder.getType() & folder.HOLDS_FOLDERS) != 0) {
            Folder[] subFolders = folder.list();
            for (Folder sub : subFolders) {
                expandFolder(sub, l);
            }
        }
        if ((folder.getType() & folder.HOLDS_MESSAGES) != 0) {
            folder.open(Folder.READ_ONLY);
            int count = folder.getMessageCount();
            Message[] messages = folder.getMessages(count - 10, count);
            for (Message msg : messages) {
                showMsgTitle(msg, l);
            }
            folder.close(false);
        }
    }

    private static void showMsgTitle(Message message, int l) throws MessagingException, UnsupportedEncodingException {
        String pre = StringUtils.leftPad("|- ", l * 4 + 3);
        String tabs = StringUtils.leftPad("", l * 4 + 3);
        Address[] addresses = message.getFrom();
        String subject = message.getSubject();
        subject = StringUtils.replace(subject, "=?", " =?");
        subject = MimeUtility.decodeText(subject);
        Date date = message.getSentDate();
        StringBuilder out = new StringBuilder();
        out.append(pre).append("主题:").append(subject);
        out.append("\n").append(tabs).append("來至:");
        for (Address address : addresses) {
            String from = address.toString();
            from = MimeUtility.decodeText(from);
            out.append(" ").append(from);
        }
        DateTime dt = new DateTime(date);
        out.append("\n").append(tabs).append("送件时间:").append(dt.toString("yyyy-MM-dd HH:mm:ss"));
        System.out.println(out);
    }

    @Test
    public void testSendMail() {
        Client.sendMessage("测试主题","测试内容","发送人姓名","发送人手机","nikytwo@21cn.com");
    }
}
