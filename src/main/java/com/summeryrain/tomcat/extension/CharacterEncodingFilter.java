package com.summeryrain.tomcat.extension;

import org.apache.commons.lang.BooleanUtils;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by laijie on 15-6-11.
 */
public class CharacterEncodingFilter implements Filter {

    private String encoding;

    private boolean forceEncoding = false;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.encoding = filterConfig.getInitParameter("encoding");
        this.forceEncoding = BooleanUtils.toBoolean(filterConfig.getInitParameter("forceEncoding"));
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (this.encoding != null && (this.forceEncoding || request.getCharacterEncoding() == null)) {
            request.setCharacterEncoding(this.encoding);
            if (this.forceEncoding) {
                response.setCharacterEncoding(this.encoding);
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
