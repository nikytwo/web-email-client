package com.summeryrain.email;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeUtility;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by laijie on 15-5-25.
 */
public class Client {
    public static final String CHARSET = "UTF-8";
    private static Properties properties = new Properties();
    private static String inbox = Option.getOpt().getInbox();
    private static Option option = Option.getOpt();

    public static Folder getInbox() {
        Session session = Session.getDefaultInstance(properties);
        Folder folder = null;
        if (!option.validate()) {
            return null;
        }
        try {
            Store store = session.getStore(option.getProtocol());
            store.connect(option.getHost(), option.getUsername(), option.getPassword());
            folder = store.getFolder(inbox);
            folder.open(Folder.READ_WRITE);
        } catch (MessagingException e) {

        } finally {
//            store.close();
        }

        return folder;
    }

    /**
     　　*　解析邮件，把得到的邮件内容保存到一个StringBuffer对象中，解析邮件
     　　*　主要是根据MimeType类型的不同执行不同的操作，一步一步的解析
     　　*/
    public static StringBuffer getMailContent(Part part) throws Exception {
        StringBuffer bodytext = new StringBuffer();
        String contenttype = part.getContentType();
        int nameindex = contenttype.indexOf("name");
        boolean conname = false;
        if (nameindex != -1) conname = true;
        if (part.isMimeType("text/plain") && !conname) {
            bodytext.append("<pre>");
            bodytext.append((String) part.getContent());
            bodytext.append("</pre>");
        } else if (part.isMimeType("text/html") && !conname) {
            bodytext.append((String) part.getContent());
        } else if (part.isMimeType("multipart/*")) {
            Multipart multipart = (Multipart) part.getContent();
            int counts = multipart.getCount();
            for (int i = 0; i < counts; i++) {
                bodytext.append(getMailContent(multipart.getBodyPart(i)));
            }
        } else if (part.isMimeType("message/rfc822")) {
            bodytext.append(getMailContent((Part) part.getContent()));
        } else {
            bodytext.append((String) part.getContent());
        }

        return bodytext;
    }

    public static void sendMessage(String subject, String content, String name, String mobile, String replyEmail) {
        try {
            Email email = new SimpleEmail();
            email.setSSLOnConnect(option.isSmtpSslOn());
            email.setHostName(option.getSmtp());
            email.setSmtpPort(option.getSmtpPort());
            email.setAuthentication(option.getUsername(), option.getPassword());
            email.setCharset(CHARSET);
            email.setFrom(option.getUsername());
            email.addReplyTo(replyEmail);
            email.addTo(option.getUsername());
            email.setSubject(subject);
            StringBuilder sbContent = buildContent(content, name, mobile, replyEmail);
            email.setMsg(sbContent.toString());
            email.send();
        } catch (EmailException e) {
            e.printStackTrace();
        }
    }

    private static StringBuilder buildContent(String content, String name, String mobile, String replyEmail) {
        StringBuilder sbContent = new StringBuilder(content);
        sbContent.append("\n\n")
                .append("发送人姓名：").append(name).append("\n")
                .append("发送人手机：").append(mobile).append("\n")
                .append("发送人信箱：").append(replyEmail).append("\n");
        return sbContent;
    }

    public static void replyTo(Message message, String content) {
        try {
            Email email = new SimpleEmail();
            email.setSSLOnConnect(option.isSmtpSslOn());
            email.setHostName(option.getSmtp());
            email.setSmtpPort(option.getSmtpPort());
            email.setAuthentication(option.getUsername(), option.getPassword());
            String subject = message.getSubject();
            subject = StringUtils.replace(subject, "=?", " =?");
            subject = "回复：" + MimeUtility.decodeText(subject);
            email.setCharset(CHARSET);
            email.setFrom(option.getUsername());
            List<InternetAddress> tos = new ArrayList<InternetAddress>();
            for (Address address : message.getReplyTo()) {
                tos.add((InternetAddress) address);
            }
            if (tos.size() <= 0) {
                for (Address address : message.getFrom()) {
                    tos.add((InternetAddress) address);
                }
            }
            email.setTo(tos);
            email.setSubject(subject);
            StringBuilder sbContent = new StringBuilder(content);
            sbContent.append("\n\n");
            email.setMsg(sbContent.toString());
            email.send();
        } catch (EmailException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static Message getMessageBy(int id) {
        Folder inbox = getInbox();
        if (null == inbox) {
            return null;
        }
        try {
            return inbox.getMessage(id);
        } catch (MessagingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
