package com.summeryrain.email;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * Created by laijie on 15-6-11.
 */
public class Option {
    private static Logger logger = Logger.getLogger(Option.class.getName());

    private static final Option me = new Option();
    public static final String PROPERTIES_FILE = "/email.properties";
    public static final String EMAIL_RECEIVE_INBOX = "email.receive.inbox";
    public static final String EMAIL_RECEIVE_PROTOCOL = "email.receive.protocol";
    public static final String EMAIL_RECEIVE_HOST = "email.receive.host";
    public static final String EMAIL_RECEIVE_PORT = "email.receive.port";
    public static final String EMAIL_RECEIVE_SSL = "email.receive.ssl";
    public static final String EMAIL_SEND_HOST = "email.send.host";
    public static final String EMAIL_SEND_PORT = "email.send.port";
    public static final String EMAIL_SEND_SSL = "email.send.ssl";
    public static final String EMAIL_USERNAME = "email.username";
    public static final String EMAIL_PASSWORD = "email.password";
    public static final String DEFAULT_RECEIVE_PROTOCOL = "pop3";
    public static final String DEFAULT_RECEIVE_INBOX = "INBOX";
    public static final String DEFAULT_SMTP_PORT = "25";
    public static final String DEFAULT_POP_PORT = "110";
    public static final String DEFAULT_IMAP_PORT = "143";
    public static final String DEFAULT_SSL_SMTP_PORT = "465";
    public static final String DEFAULT_SSL_POP_PORT = "995";
    public static final String DEFAULT_SSL_IMAP_PORT = "993";

    private String inbox;
    private String protocol;
    private String host;
    private boolean sslOn;
    private int port;
    private String smtp;
    private boolean smtpSslOn;
    private int smtpPort;
    private String username;
    private String password;

    public static Option getOpt() {
        return me;
    }

    public Option() {
        load();
    }

    private void load() {
        InputStream in = this.getClass().getResourceAsStream(PROPERTIES_FILE);
        Properties p = new Properties();
        try {
            p.load(in);
            this.setInbox(p.getProperty(EMAIL_RECEIVE_INBOX));
            this.setProtocol(p.getProperty(EMAIL_RECEIVE_PROTOCOL));
            this.setHost(p.getProperty(EMAIL_RECEIVE_HOST));
            this.setSslOn(p.getProperty(EMAIL_RECEIVE_SSL));
            this.setPort(p.getProperty(EMAIL_RECEIVE_PORT));
            this.setSmtp(p.getProperty(EMAIL_SEND_HOST));
            this.setSmtpSslOn(p.getProperty(EMAIL_SEND_SSL));
            this.setSmtpPort(p.getProperty(EMAIL_SEND_PORT));
            this.setUsername(p.getProperty(EMAIL_USERNAME));
            this.setPassword(p.getProperty(EMAIL_PASSWORD));
        } catch (IOException e) {
            logger.error("加载“" + PROPERTIES_FILE + "”文件出错。\n" + e);
        }
    }

    public void save() {
        Properties p = new Properties();

        try {
            p.put(EMAIL_RECEIVE_INBOX, this.getInbox());
            p.put(EMAIL_RECEIVE_PROTOCOL, this.getProtocol());
            p.put(EMAIL_RECEIVE_HOST, this.getHost());
            p.put(EMAIL_RECEIVE_SSL, String.valueOf(this.isSslOn()));
            p.put(EMAIL_RECEIVE_PORT, String.valueOf(this.getPort()));
            p.put(EMAIL_SEND_HOST, this.getSmtp());
            p.put(EMAIL_SEND_SSL, String.valueOf(this.isSmtpSslOn()));
            p.put(EMAIL_SEND_PORT, String.valueOf(this.getSmtpPort()));
            p.put(EMAIL_USERNAME, this.getUsername());
            p.put(EMAIL_PASSWORD, this.getPassword());
            URI uri = this.getClass().getResource(PROPERTIES_FILE).toURI();
            p.store(new FileOutputStream(uri.getPath()), "web email client settings");
        } catch (URISyntaxException e) {
            logger.error("获取“" + PROPERTIES_FILE + "”文件路径出错。\n" + e);
        } catch (IOException e) {
            logger.error("保存“" + PROPERTIES_FILE + "”文件出错。\n" + e);
        }
    }

    // 校验设置，暂只校验必填项
    public boolean validate() {
        return StringUtils.isNotEmpty(this.host)
                && StringUtils.isNotEmpty(this.protocol)
                && StringUtils.isNotEmpty(this.smtp)
                && StringUtils.isNotEmpty(this.username)
                && StringUtils.isNotEmpty(this.password);
    }

    public String getInbox() {
        return inbox;
    }

    public void setInbox(String inbox) {
        this.inbox = StringUtils.defaultString(inbox, DEFAULT_RECEIVE_INBOX);
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public boolean isSmtpSslOn() {
        return smtpSslOn;
    }

    public void setSmtpSslOn(String smtpSslOn) {
        this.smtpSslOn = BooleanUtils.toBoolean(smtpSslOn);
    }

    public void setSmtpSslOn(boolean smtpSslOn) {
        this.smtpSslOn = smtpSslOn;
    }

    public boolean isSslOn() {
        return sslOn;
    }

    public void setSslOn(String sslOn) {
        this.sslOn = BooleanUtils.toBoolean(sslOn);
    }

    public void setSslOn(boolean sslOn) {
        this.sslOn = sslOn;
    }

    public int getPort() {
        return port;
    }

    public void setPort(String port) {
        if (this.isSslOn()) {
            if (DEFAULT_RECEIVE_PROTOCOL.equalsIgnoreCase(this.protocol)) {
                this.port = Integer.parseInt(StringUtils.defaultString(port, DEFAULT_SSL_POP_PORT));
            } else {
                this.port = Integer.parseInt(StringUtils.defaultString(port, DEFAULT_SSL_IMAP_PORT));
            }
        } else {
            if (DEFAULT_RECEIVE_PROTOCOL.equalsIgnoreCase(this.protocol)) {
                this.port = Integer.parseInt(StringUtils.defaultString(port, DEFAULT_POP_PORT));
            } else {
                this.port = Integer.parseInt(StringUtils.defaultString(port, DEFAULT_IMAP_PORT));
            }
        }

    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = StringUtils.defaultString(protocol, DEFAULT_RECEIVE_PROTOCOL);
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public int getSmtpPort() {
        return smtpPort;
    }

    public void setSmtpPort(String smtpPort) {
        if (this.isSmtpSslOn()) {
            this.smtpPort = Integer.parseInt(StringUtils.defaultString(smtpPort, DEFAULT_SSL_SMTP_PORT));
        } else {
            this.smtpPort = Integer.parseInt(StringUtils.defaultString(smtpPort, DEFAULT_SMTP_PORT));
        }
    }

    public void setSmtpPort(int smtpPort) {
        this.smtpPort = smtpPort;
    }
}
