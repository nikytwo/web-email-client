<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="com.summeryrain.email.Option" %>
<%--
  Created by IntelliJ IDEA.
  User: laijie
  Date: 15-6-11
  Time: 上午11:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String receiveProtocol = StringUtils.defaultString(request.getParameter("receive_protocol"));
    String receiveHost = StringUtils.defaultString(request.getParameter("receive_host"));
    String receivePort = StringUtils.defaultString(request.getParameter("receive_port"));
    String sendHost = StringUtils.defaultString(request.getParameter("send_host"));
    String sendPort = StringUtils.defaultString(request.getParameter("send_port"));
    String username = StringUtils.defaultString(request.getParameter("username"));
    String password = StringUtils.defaultString(request.getParameter("password"));
    Option opt = Option.getOpt();

    if (StringUtils.isNotEmpty(receiveProtocol)
            && StringUtils.isNotEmpty(receiveHost)
            && StringUtils.isNotEmpty(sendHost)
            && StringUtils.isNotEmpty(username)
            && StringUtils.isNotEmpty(password)) {
        opt.setProtocol(receiveProtocol);
        opt.setHost(receiveHost);
        opt.setPort(receivePort);
        opt.setSmtp(sendHost);
        opt.setSmtpPort(sendPort);
        opt.setUsername(username);
        opt.setPassword(password);
        opt.save();
        response.sendRedirect("email-list.jsp");
        return;
    }
%>
<html>
<head>
    <title>邮件>>配置</title>
    <link rel="stylesheet" href="static/css/site.css">
    <link rel="stylesheet" href="static/css/font-awesome.min.css">
    <link rel="stylesheet" media="screen" href="./static/css/screen.css">
    <script src="./static/js/jquery/jquery-1.6.4.js"></script>
    <script src="./static/js/jquery.validate.js"></script>
</head>
<body>
<div>
    <form class="cmxform" id="settingForm" method="post" action="email-setting.jsp" novalidate="novalidate">
        <fieldset>
            <legend>邮箱客户端设置</legend>
            <fieldset>
                <legend>收件箱设置</legend>
            <p>
                <label for="receive_protocol">协议：</label>
                <select id="receive_protocol" name="receive_protocol" >
                    <option value="pop3" selected>POP3</option>
                    <option value="imap">IMAP</option>
                </select>
            </p>
            <p>
                <label for="receive_host">服务器地址：</label>
                <input id="receive_host" name="receive_host" type="text">
            </p>
            <p>
                <label for="receive_port">服务器端口：</label>
                <input id="receive_port" name="receive_port" type="text">
            </p>
            </fieldset>
            <fieldset>
                <legend>发件箱设置</legend>
            <p>
                <label for="send_protocol">协议：</label>
                <select id="send_protocol" name="send_protocol" >
                    <option value="smtp" selected>SMTP</option>
                </select>
            </p>
            <p>
                <label for="send_host">服务器地址：</label>
                <input id="send_host" name="send_host" type="text">
            </p>
            <p>
                <label for="send_port">服务器端口：</label>
                <input id="send_port" name="send_port" type="text">
            </p>
            </fieldset>
            <fieldset>
                <legend>账户设置</legend>
            <p>
                <label for="username">用户名：</label>
                <input type="text" id="username" name="username">
            </p>
            <p>
                <label for="password">密码：</label>
                <input type="password" id="password" name="password">
            </p>
            </fieldset>
            <p>
                <input class="submit btn btn-default" type="submit" value="保存">
                <input class="btn btn-default" type="reset" value="重置">
                <input class="btn btn-default" type="button" value="返回" onclick="history.back();">
            </p>
        </fieldset>
    </form>
</div>
<script>
    $(document).ready(function() {

        // 初始化表单验证
        $("#settingForm").validate({
            rules: {
                receive_protocol: {
                    required: true
                },
                receive_host: {
                    required: true
                },
                receive_port: {
                    required: true,
                    range: [0, 65536],
                    digits: true
                },
                send_protocol: {
                    required: true
                },
                send_host: {
                    required: true
                },
                send_port: {
                    required: true,
                    range: [0, 65536],
                    digits: true
                },
                username: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                receive_protocol: {
                    required: "不能为空"
                },
                receive_host: {
                    required: "不能为空"
                },
                receive_port: {
                    required: "不能为空",
                    range: "请输入0至65536的端口号",
                    digits: "请输入0至65536的端口号"
                },
                send_protocol: {
                    required: "不能为空"
                },
                send_host: {
                    required: "不能为空"
                },
                send_port: {
                    required: "不能为空",
                    range: "请输入0至65536的端口号",
                    digits: "请输入0至65536的端口号"
                },
                username: {
                    required: "不能为空",
                    email: "邮箱格式应为name@domain.com"
                },
                password: {
                    required: "不能为空"
                }
            }
        });

        // 初始化表单事件
        var defaultPOP3Port = "<%=Option.DEFAULT_POP_PORT%>";
        var defaultIMAPPort = "<%=Option.DEFAULT_IMAP_PORT%>";
        var defaultSMTPPort = "<%=Option.DEFAULT_SMTP_PORT%>";
        var defaultSSLPOP3Port = "<%=Option.DEFAULT_SSL_POP_PORT%>";
        var defaultSSLIMAPPort = "<%=Option.DEFAULT_SSL_IMAP_PORT%>";
        var defaultSSLSMTPPort = "<%=Option.DEFAULT_SSL_SMTP_PORT%>";

        var emReceiveProtocol = $("#receive_protocol");
        emReceiveProtocol.change(function() {
            var emReceivePort = $('#receive_port');
            if ("pop3" == $(this).val()) {
                emReceivePort.val(defaultPOP3Port);
            } else if ("imap" == $(this).val()) {
                emReceivePort.val(defaultIMAPPort);
            }
        });

        var emSendProtocol = $('#send_protocol');
        emSendProtocol.change(function() {
            var sendPort = $('#send_port');
            if ("pop3" == $(this).val()) {
                sendPort.val(defaultSMTPPort);
            }
        });

        // 加载表单数据
        var receiveProtocol = "<%=opt.getProtocol()%>";
        var receiveHost = "<%=opt.getHost()%>";
        var receivePort = "";
        var sendProtocol = "smtp";
        var sendHost = "<%=opt.getSmtp()%>";
        var sendPort = "<%=opt.getSmtpPort()%>";
        var username = "<%=opt.getUsername()%>";
        var password = "<%=opt.getPassword()%>";
        emReceiveProtocol.val(receiveProtocol)
                .change();
        $('#receive_host').val(receiveHost);
        if ("" != receivePort) {
            $('#receive_port').val(receivePort);
        }
        emSendProtocol.val(sendProtocol)
                .change();
        $('#send_host').val(sendHost);
        if ("" != sendPort) {
            $('#send_port').val(sendPort);
        }
        $('#username').val(username);
        $('#password').val(password);
    });
</script>
</body>
</html>
