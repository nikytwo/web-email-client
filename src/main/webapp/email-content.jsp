<%@ page import="com.summeryrain.email.Client" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="javax.mail.internet.MimeUtility" %>
<%@ page import="java.util.Date" %>
<%@ page import="org.joda.time.DateTime" %>
<%@ page import="javax.mail.*" %>
<%--
  Created by IntelliJ IDEA.
  User: laijie
  Date: 15-6-10
  Time: 上午9:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String id = request.getParameter("id");
    String subject = "无邮件";
    String strFrom = "";
    String strDate = "";
    Message message = null;
    if (StringUtils.isNotEmpty(id)) {
        Folder inbox = Client.getInbox();
        message = inbox.getMessage(Integer.parseInt(id));
        subject = message.getSubject();
        subject = StringUtils.replace(subject, "=?", " =?");
        subject = MimeUtility.decodeText(subject);
        Address[] addresses = message.getFrom();
        for (Address address : addresses) {
            String from = address.toString();
            from = MimeUtility.decodeText(from);
            if (StringUtils.isNotEmpty(strFrom)) {
                strFrom += ", ";
            }
            from = from.replace("<", "&lt;").replaceAll(">", "&gt;");
            strFrom += from;
        }
        Date date = message.getSentDate();
        DateTime dt = new DateTime(date);
        strDate = dt.toString("yyyy-MM-dd HH:mm:ss");
//        try {
//            message.setFlag(Flags.Flag.SEEN, true);
//            message.saveChanges();
//        } catch (MessagingException e) {
//            e.printStackTrace();
//        }
    }
%>
<html>
<head>
    <title>邮件>><%=subject%></title>
    <link rel="stylesheet" href="static/css/site.css">
    <link rel="stylesheet" href="static/css/font-awesome.min.css">
</head>
<body>
<div id="toolbar">
    <a class="btn btn-default" href="" title="返回邮件列表" onclick="history.back();">
        <i class="fa fa-arrow-circle-o-left"> 返回</i>
    </a>
    <a class="btn default btn-primary" href="email-reply.jsp?id=<%=id%>">
        <i class="fa fa-reply"> 回复</i>
    </a>
</div>
<br>
<h2 id="subject"><%=subject%></h2>
<h4 id="from"><span>发件人：</span><span><%=strFrom%></span></h4>
<h4 id="date"><span>时间：</span><span><%=strDate%></span></h4>
<br>
<p id="content">
    <%
        if (null != message) {
            out.write(Client.getMailContent(message).toString());
        }
    %>
</p>
</body>
</html>
