<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="com.summeryrain.email.Client" %>
<%--
  Created by IntelliJ IDEA.
  User: laijie
  Date: 15-6-10
  Time: 上午11:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String subject = StringUtils.defaultString(request.getParameter("subject"));
    String content = StringUtils.defaultString(request.getParameter("content"));
    String name = StringUtils.defaultString(request.getParameter("name"));
    String mobile = StringUtils.defaultString(request.getParameter("mobile"));
    String email = StringUtils.defaultString(request.getParameter("email"));

    if (StringUtils.isNotEmpty(subject)
            && StringUtils.isNotEmpty(content)
            && StringUtils.isNotEmpty(name)
            && StringUtils.isNotEmpty(mobile)
            && StringUtils.isNotEmpty(email)) {
        Client.sendMessage(subject, content, name, mobile, email);
    }
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>邮件>>发邮件</title>
    <link rel="stylesheet" href="static/css/site.css">
    <link rel="stylesheet" href="static/css/font-awesome.min.css">
    <link rel="stylesheet" media="screen" href="./static/css/screen.css">
    <script src="./static/js/jquery/jquery-1.6.4.js"></script>
    <script src="./static/js/jquery.validate.js"></script>
</head>
<body>
<div>
    <form class="cmxform" id="settingForm" method="post" action="email-send.jsp" novalidate="novalidate">
        <fieldset>
            <legend>发送邮件</legend>
            <p>
                <label for="subject">主题：</label>
                <input id="subject" name="subject" type="text" style="width:305px;" value="<%=subject%>">
                <label class="error">*</label>
            </p>
            <p>
                <label for="content">内容：</label>
                <textarea id="content" name="content" cols="47" rows="9"><%=content%></textarea>
                <label class="error">*</label>
            </p>
            <p>
                <label for="name">姓名：</label>
                <input id="name" name="name" type="text">
                <label class="error">*</label>
            </p>
            <p>
                <label for="mobile">手机号码：</label>
                <input id="mobile" name="mobile" type="text">
                <label class="error">*</label>
            </p>
            <p>
                <label for="email">电子邮箱：</label>
                <input id="email" name="email" type="text">
                <label class="error">*</label>
            </p>
            <p>
                <input class="submit btn btn-default" type="submit" value="发送">
            </p>
        </fieldset>
    </form>
</div>
<script>
    $.validator.addMethod("mobile", function (value, element) {
        return this.optional(element) || /^0?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/.test(value);
    }, "请输入正确的手机号码，便于我们通过电话与您联系和回复");

    $(document).ready(function () {
        $("#settingForm").validate({
            rules: {
                subject: {
                    required: true
                },
                content: {
                    required: true
                },
                name: {
                    required: true
                },
                mobile: {
                    required: true,
                    mobile: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                subject: {
                    required: "不能为空"
                },
                content: {
                    required: "不能为空"
                },
                name: {
                    required: "不能为空"
                },
                mobile: {
                    required: "不能为空"
                },
                email: {
                    required: "不能为空",
                    email: "请输入正确的邮箱地址，便于我们通过邮箱与您联系和回复"
                }
            }
        });
    });
</script>
</body>
</html>
