<%@ page import="javax.mail.Message" %>
<%@ page import="javax.mail.Folder" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="javax.mail.internet.MimeUtility" %>
<%@ page import="com.summeryrain.email.Client" %>
<%@ page import="java.util.Date" %>
<%@ page import="org.joda.time.DateTime" %>
<%--
  Created by IntelliJ IDEA.
  User: laijie
  Date: 15-6-4
  Time: 下午4:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    int pageNO = 1;
    int pageCount = 30;
    String curURI = request.getRequestURI();
    String settingURI = "email-setting.jsp";
    String baseContentURI = "email-content.jsp";

    String page_no = request.getParameter("page_no");
    if (StringUtils.isNotEmpty(page_no)) {
        pageNO = Integer.parseInt(page_no);
//        pageNO = pageNO > 0 ? pageNO : 1;
    }

    String page_count = request.getParameter("page_count");
    if (StringUtils.isNotEmpty(page_count)) {
        pageCount = Integer.parseInt(page_count);
    }

    Folder inbox = Client.getInbox();
    if (null == inbox) {
        // 无法登录则跳转至设置页面
        response.sendRedirect(settingURI);
        return;
    }
    int count = inbox.getMessageCount();
    int start = count - (pageNO - 1) * pageCount;
    if (start < 0 || start > count) {
        // 页码超出范围，跳转至首页
        response.sendRedirect(curURI);
        return;
    }
    int end = start - pageCount;
    end = end > 0 ? end : 0;

    String prePageURI = curURI +"?page_no=" + String.valueOf(pageNO - 1);
    String nextPageURI = curURI +"?page_no=" + String.valueOf(pageNO + 1);
%>
<html>
<head>
    <title>邮件>>收件箱</title>
    <link rel="stylesheet" href="static/css/site.css">
    <link rel="stylesheet" href="static/css/tablesaw.css">
    <link rel="stylesheet" href="static/css/font-awesome.min.css">
    <!--[if lt IE 9]>
    <script src="static/js/respond/respond.min.js"></script>
    <!--<![endif]-->
    <script src="static/js/tablesaw.js"></script>
</head>
<body>
<div id="toolbar">
    <a class="btn btn-default" href="<%=settingURI%>"><i class="fa fa-cog">邮箱设置</i></a>
    <a class="btn btn-default" href="<%=prePageURI%>"><i class="fa fa-chevron-left">上一页</i></a>
    <a class="btn btn-default" href="<%=nextPageURI%>"><i class="fa fa-chevron-right">下一页</i></a>
</div>
<br>
<div id="content">
    <table class="tablesaw tablesaw-stack" data-tablesaw-mode="stack">
        <thead>
        <tr>
            <td>主题</td>
            <td>时间</td>
        </tr>
        </thead>
        <tbody>
        <%
            for (int id = start; id > end; id--) {
                Message message = inbox.getMessage(id);
                StringBuilder sb = new StringBuilder();
                String subject = message.getSubject();
                subject = StringUtils.defaultString(subject, "无主题").replace("=?", " =?");
                subject = MimeUtility.decodeText(subject);
                Date date = message.getSentDate();
                DateTime dt = new DateTime(date);
                String contentURI = baseContentURI + "?id=" + id;
                sb.append("<tr>")
                        .append("<td><a href='").append(contentURI).append("'>")
                        .append(subject)
                        .append("</a></td>")
                        .append("<td>")
                        .append(dt.toString("yyyy-MM-dd HH:mm:ss"))
                        .append("</td>")
                        .append("</tr>");
                out.write(sb.toString());
            }
        %>
        </tbody>
    </table>
</div>
<br>
<div id="nav_bar">
    <a class="btn btn-default" href="<%=prePageURI%>"><i class="fa fa-chevron-left">上一页</i></a>
    <a class="btn btn-default" href="<%=nextPageURI%>"><i class="fa fa-chevron-right">下一页</i></a>
</div>
</body>
</html>
