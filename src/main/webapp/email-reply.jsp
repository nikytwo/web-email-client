<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="com.summeryrain.email.Client" %>
<%@ page import="javax.mail.Message" %>
<%--
  Created by IntelliJ IDEA.
  User: laijie
  Date: 15-6-10
  Time: 上午11:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String id = StringUtils.defaultString(request.getParameter("id"));
    String content = StringUtils.defaultString(request.getParameter("content"));

    if (StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(content)) {
        Message message = Client.getMessageBy(Integer.parseInt(id));
        Client.replyTo(message, content);
        response.sendRedirect("email-list.jsp");
    }
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>邮件>>回复邮件</title>
    <link rel="stylesheet" href="static/css/site.css">
    <link rel="stylesheet" href="static/css/font-awesome.min.css">
    <link rel="stylesheet" media="screen" href="./static/css/screen.css">
    <script src="./static/js/jquery/jquery-1.6.4.js"></script>
    <script src="./static/js/jquery.validate.js"></script>
</head>
<body>
<div>
    <form class="cmxform" id="settingForm" method="post" action="email-reply.jsp" novalidate="novalidate">
        <fieldset>
            <legend>回复邮件</legend>
            <p>
                <input id="id" name="id" type="hidden" value="<%=id%>">
            </p>
            <p>
                <label for="content">内容：</label>
                <textarea id="content" name="content" cols="47" rows="9"><%=content%></textarea>
            </p>
            <p>
                <input class="submit btn btn-default" type="submit" value="发送">
                <input class="btn btn-default" type="button" value="返回" onclick="history.back();">
            </p>
        </fieldset>
    </form>
</div>
<script>

    $(document).ready(function () {
        $("#settingForm").validate({
            rules: {
                content: {
                    required: true
                }
            },
            messages: {
                content: {
                    required: "不能为空"
                }
            }
        });
    });
</script>
</body>
</html>
